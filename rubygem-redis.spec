%global gem_name redis
Name:                rubygem-%{gem_name}
Version:             4.7.0
Release:             4
Summary:             A Ruby client library for Redis
License:             MIT
URL:                 https://github.com/redis/redis-rb
Source0:             https://rubygems.org/gems/%{gem_name}-%{version}.gem
Source1:             https://github.com/redis/redis-rb/archive/refs/tags/v%{version}.tar.gz
BuildRequires:       ruby ruby(release) rubygems-devel rubygem(test-unit) redis5 make rubygem(hiredis) rubygem(minitest) rubygem(mocha) procps-ng
BuildArch:           noarch
%description
A Ruby client that tries to match Redis' API one-to-one, while still
providing an idiomatic interface.

%package doc
Summary:             Documentation for %{name}
Requires:            %{name} = %{version}-%{release}
BuildArch:           noarch
%description doc
Documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version} -b 1

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
cp -a %{_builddir}/redis-rb-%{version}/{makefile,test} .
sed -i "s/bundle exec rake test/ruby -Ilib:test -e \"Dir.glob('.\/test\/**\/*_test.rb').sort.each {|t| require t}\"/" \
  makefile

# The TestInternals#test_large_payload fails with Hiredis.
# https://github.com/redis/redis-rb/issues/1117
sed -i \
  -e '/def test_large_payload/i\  driver(:ruby) do' \
  -e '/def test_large_payload/,/end/ {/end/ s/end/&\n  end/;}' \
  test/internals_test.rb

sed -i \
  -e '/def test_redirection_when_slot_is_resharding/i\  driver(:ruby) do' \
  -e '/def test_redirection_when_slot_is_resharding/,/end/ {/end/ s/end/&\n  end/;}' \
  test/cluster_client_slots_test.rb

sed -i 's/redis:\/\/\//redis:\/\/127.0.0.1/g' test/url_param_test.rb

mkdir bin
echo '#!/usr/bin/sh' > bin/build
chmod a+x bin/build
mv %{_builddir}/redis-rb-%{version}/bin/cluster_creator bin/
LANG=C.UTF-8
pkill -f redis-server || true
sleep 5
for driver in ruby hiredis ; do
export DRIVER=$driver
make BINARY=$(which redis-server) REDIS_CLIENT=$(which redis-cli) BUILD_DIR='${TMP}'
sleep 5
done
popd


%files
%dir %{gem_instdir}
%{gem_libdir}
%license %{gem_instdir}/LICENSE
%{gem_spec}
%exclude %{gem_cache}
%exclude %{gem_instdir}/.*

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_instdir}/README.md

%changelog
* Fri Feb 14 2025 wangkai <13474090681@163.com> - 4.7.0-4
- Switch buildrequire redis to redis5

* Sat Aug 12 2023 Ge Wang <wang__ge@126.com> - 4.7.0-3
- Fix test_redirection_when_slot_is_resharding test failure

* Thu Jan 12 2023 xu_ping <xuping33@h-partners.com> - 4.7.0-2
- The TestInternals#test_large_payload fails with hiredis.

* Tue Aug 9 2022 liyanan <liyanan32@h-partners.com> - 4.7.0-1
- Update to 4.7.0

* Mon Jul 18 2022 wulei <wulei80@h-partners.com> - 4.6.0-2
- Fix the compilation failure

* Thu Jun 30 2022 wulei <wulei80@h-partners.com> - 4.6.0-1
- Upgrade to 4.6.0

* Tue Aug 18 2020 huangyangke <huangyangke@huawei.com> - 4.0.1-1
- package init
